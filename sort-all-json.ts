#!/usr/bin/env -S deno run
import { join } from "https://deno.land/std@0.106.0/path/mod.ts"

async function sortAllJsonFiles(dir: string) {
  await Promise.all([
    Deno.permissions.request({ name: "read", path: dir }),
    Deno.permissions.request({ name: "write", path: dir }),
  ])

  for await (const ent of Deno.readDir(dir)) {
    if (ent.isFile && ent.name.endsWith(".json")) {
      const entPath = join(dir, ent.name)
      const contents = JSON.parse(await Deno.readTextFile(entPath))
      const sortedKeys = Object.keys(contents).sort()
      const sortedContents = Object.fromEntries(
        sortedKeys.map((key) => [key, contents[key]])
      )
      await Deno.writeTextFile(
        entPath,
        `${JSON.stringify(sortedContents, null, 2)}\n`
      )
    }
  }
}

if (import.meta.main) {
  await Deno.permissions.request({ name: "read" })
  await sortAllJsonFiles(Deno.cwd())
}
